resource "aws_key_pair" "keyPair" {
    key_name = "exampleKey"
    public_key = file("~/.ssh/id_rsa.pub")
}
resource "aws_instance" "TpNumbTwo" {
  ami           = "ami-0915bcb5fa77e4892"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.keyPair.key_name
provisioner "local-exec" {
    command = "echo ${aws_instance.TpNumbTwo.public_ip} > ip_adress.txt"
  } 
connection {
      type = "ssh"
      user = "ec2-user"
      private_key = file("~/.ssh/id_rsa")
      host = self.public_ip 
  }
provisioner "remote-exec" {
    inline = [
        "sudo yum -y update",
        "sudo yum -y install httpd",
        "sudo systemctl enable httpd",
        "sudo systemctl start httpd"
      ]
  }
}